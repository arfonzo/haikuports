SUMMARY="A tool for downloading files from the internet"
DESCRIPTION="
wget is a tool that can download files from the internet through protocols \
such as HTTP, HTTPS and FTP. wget is non-interactive, so it can be called from \
scripts.
"
HOMEPAGE="http://www.gnu.org/software/wget" 
SRC_URI="http://ftp.gnu.org/gnu/wget/wget-1.14.tar.gz"
CHECKSUM_SHA256="f3a6898e3a765bb94435b04a6668db9e5d19b3e90e0c69a503a2773ae936c269"
LICENSE="GNU GPL v3"
COPYRIGHT="1996-2012 Free Software Foundation"
REVISION="2"
ARCHITECTURES="x86_gcc2 ?x86 ?x86_64"
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	wget$secondaryArchSuffix = $portVersion
	cmd:wget$secondaryArchSuffix = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libz$secondaryArchSuffix >= 1.2.3
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:aclocal
	cmd:autoconf
	cmd:libtoolize
	cmd:make
	cmd:makeinfo
	cmd:pod2man
	"

USER_SETTINGS_FILES="
	settings/wgetrc
	"

GLOBAL_WRITABLE_FILES="
	settings/wgetrc keep-old
"

PATCHES="
	wget-1.14.patch
	wget-1.14-build-fixes.patch
	wget-1.14-pod-fixes.patch
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal -I m4
	autoconf
	runConfigure ./configure --with-ssl=openssl
	make
}

INSTALL()
{
	make install
	# Tells wget where to find certificates
	echo "ca_certificate=/boot/system/data/ssl/CARootCertificates.pem" > $settingsDir/wgetrc
}

TEST()
{
	[ -d .git ] && mv .git .git-temp
	make check || true
	[ -d .git-temp ] && mv .git-temp .git
}
