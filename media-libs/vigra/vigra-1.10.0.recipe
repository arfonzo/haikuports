SUMMARY="Vision with Generic Algorithms"
DESCRIPTION="
C++ computer vision library with emphasize on customizable algorithms \
and data structures.
"
LICENSE="MIT"
COPYRIGHT="1998-2013 by Ullrich Koethe"
HOMEPAGE="http://ukoethe.github.io/vigra/"
SRC_URI="https://github.com/ukoethe/vigra/archive/Version-1-10-0.tar.gz"
SRC_FILENAME="$portName-$portVersion.tar.gz"
CHECKSUM_SHA256="406f6fcbcea2e92f681a7b844487c29049d338f5b2b25f8145e67bcb518c7ef8"
REVISION="4"

ARCHITECTURES="x86 ?x86_gcc2 ?x86_64"
SECONDARY_ARCHITECTURES="x86 ?x86_gcc2"

SOURCE_DIR="vigra-Version-1-10-0"

PROVIDES="
	vigra$secondaryArchSuffix = $portVersion
	lib:libvigraimpex$secondaryArchSuffix = 5.1.10.0 compat >= 5
"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libjpeg$secondaryArchSuffix
	lib:libpng$secondaryArchSuffix
	lib:libtiff$secondaryArchSuffix
	lib:libHalf$secondaryArchSuffix
	lib:libIex_2_1$secondaryArchSuffix
	lib:libImath_2_1$secondaryArchSuffix
	lib:libIlmImf_Imf_2_1$secondaryArchSuffix
	lib:libIlmThread_2_1$secondaryArchSuffix
	lib:libfftw$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
"
BUILD_REQUIRES="
	devel:libjpeg$secondaryArchSuffix
	devel:libpng$secondaryArchSuffix
	devel:libtiff$secondaryArchSuffix
	devel:libHalf$secondaryArchSuffix
	devel:libIex$secondaryArchSuffix
	devel:libImath$secondaryArchSuffix
	devel:libIlmImf$secondaryArchSuffix
	devel:libfftw$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:cmake
	cmd:g++$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
"

BUILD()
{
	mkdir -p build
	cd build
	cmake -DCMAKE_INSTALL_PREFIX=$prefix -DWITH_HDF5=0 -DWITH_VIGRANUMPY=0 \
		-DWITH_OPENEXR=1 ../
	make $jobArgs
}

INSTALL()
{
	cd build
	make install

	# we did not generate documentation
	rm -rf $prefix/doc

	# move headers
	mkdir -p $includeDir
	mv $prefix/include/* $includeDir

	rm -rf $prefix/include

	prepareInstalledDevelLib libvigraimpex

	# move CMake scripts...
	# FIXME: ...which probably won't work
	mv $libDir/$portName $developLibDir
	sed -i "s#/include/#/$relativeIncludeDir/#g" \
		$developLibDir/$portName/VigraConfig.cmake

	packageEntries devel $developDir bin/vigra-config
}

TEST()
{
	# FIXME: these tests fail, probably due to wrong paths
	cd build
	make test
}

PROVIDES_devel="
	vigra${secondaryArchSuffix}_devel = $portVersion
	cmd:vigra_config$secondaryArchSuffix
	devel:libvigraimpex$secondaryArchSuffix = 5.1.10.0 compat >= 5
	"
REQUIRES_devel="
	vigra$secondaryArchSuffix == $portVersion base
	cmd:python # vigra-config is python script
	"
