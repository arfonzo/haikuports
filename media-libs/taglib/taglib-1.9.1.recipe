SUMMARY="TabLib Audio Meta-Data Library"
DESCRIPTION="
TagLib Audio Meta-Data Library.
"
HOMEPAGE="http://github.com/taglib" 
SRC_URI="http://taglib.github.io/releases/taglib-1.9.1.tar.gz" 
CHECKSUM_SHA256="72d371cd1419a87ae200447a53bff2be219283071e80fd12337928cc967dc71a"
COPYRIGHT="2002-2013 Scott Wheeler"
LICENSE="GNU LGPL v2
	MPL v1.1
	"
REVISION="1"

ARCHITECTURES="x86 !x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	taglib$secondaryArchSuffix = $portVersion
	cmd:taglib_config$secondaryArchSuffix
	lib:libtag$secondaryArchSuffix = 1.9.1 compat >= 1.9
	lib:libtag_c$secondaryArchSuffix = 0.0.0 compat >= 0
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libstdc++$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	devel:libz$secondaryArchSuffix
	"

BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	cmd:gcc$secondaryArchSuffix
	cmd:cmake
	cmd:make
	"

PATCHES="taglib-1.9.1.patchset"

BUILD()
{
	cmake	-DCMAKE_INSTALL_PREFIX:PATH=$prefix \
			-DBIN_INSTALL_DIR:PATH=$binDir \
			-DLIB_INSTALL_DIR:PATH=$libDir \
			-DINCLUDE_INSTALL_DIR:PATH=$includeDir \
			.
	make
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs \
			libtag \
			libtag_c

	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$binDir
}

PROVIDES_devel="
	taglib${secondaryArchSuffix}_devel = $portVersion compat >= 1.9
	devel:libtag$secondaryArchSuffix = 1.9.1 compat >= 1.9
	devel:libtag_c$secondaryArchSuffix = 0.0.0 compat >= 0
	"

REQUIRES_devel="
	taglib$secondaryArchSuffix == $portVersion base
	"
