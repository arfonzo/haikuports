SUMMARY="C library for reading and writing files containing sampled sound"
DESCRIPTION="
libsndfile is a library of C routines for reading and writing \
files containing sampled audio data.
"
HOMEPAGE="http://www.mega-nerd.com/libsndfile"
COPYRIGHT="
	1999-2011 Erik de Castro Lopo
"
LICENSE="GNU LGPL v2.1"
SRC_URI="http://www.mega-nerd.com/libsndfile/files/libsndfile-$portVersion.tar.gz"
CHECKSUM_SHA256="59016dbd326abe7e2366ded5c344c853829bebfd1702ef26a07ef662d6aa4882"
REVISION="2"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PATCHES="libsndfile-$portVersion.patchset"

PROVIDES="
	libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	cmd:sndfile_cmp$secondaryArchSuffix
	cmd:sndfile_concat$secondaryArchSuffix
	cmd:sndfile_convert$secondaryArchSuffix
	cmd:sndfile_deinterleave$secondaryArchSuffix
	cmd:sndfile_info$secondaryArchSuffix
	cmd:sndfile_interleave$secondaryArchSuffix
	cmd:sndfile_metadata_get$secondaryArchSuffix
	cmd:sndfile_metadata_set$secondaryArchSuffix
	cmd:sndfile_play$secondaryArchSuffix
	cmd:sndfile_regtest$secondaryArchSuffix
	cmd:sndfile_salvage$secondaryArchSuffix
	lib:libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libflac$secondaryArchSuffix
	lib:libogg$secondaryArchSuffix
	lib:libvorbis$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libflac$secondaryArchSuffix
	devel:libogg$secondaryArchSuffix
	devel:libvorbis$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:find
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	libtoolize --force --copy --install
	aclocal -I M4
	autoconf
	automake
	if [ $effectiveTargetArchitecture = x86_gcc2 ]; then
		export CFLAGS=-O1
	fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libsndfile
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libsndfile${secondaryArchSuffix}_devel = $portVersion compat >= 1
	devel:libsndfile$secondaryArchSuffix = $portVersion compat >= 1
	"
REQUIRES_devel="
	libsndfile$secondaryArchSuffix == $portVersion base
	"
